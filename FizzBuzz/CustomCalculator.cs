﻿using System.Collections.Generic;

namespace FizzBuzz
{
    /// <summary>
    /// A calculator for custom variations of the fizz buzz game.
    /// </summary>
    public class CustomCalculator : IScoreCalculator
    {
        /// <summary>
        /// A dictionary where the key is the multiple to replace and the value is the replacement text.
        /// </summary>
        private IDictionary<int, string> replacements;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="replacements"></param>
        public CustomCalculator(IDictionary<int, string> replacements)
        {
            this.replacements = replacements;
        }

        /// <inheritdoc cref="IScoreCalculator.CalculateScore(int)" />
        public string CalculateScore(int number)
        {
            string result = string.Empty;

            foreach (int key in replacements.Keys)
            {
                // make a replacement when a multiple is found
                if (number.IsMultipleOf(key))
                {
                    // add the replacement text and a space
                    result = string.Concat(result, replacements[key], " ");
                }
            }
            // trim the result so any trailing space is removed
            result = result.Trim();

            // If result is empty then this is a normal number and the input should be returned.
            // Otherwise, return the result.
            return result.Length > 0 ? result : number.ToString();
        }

    }
}
