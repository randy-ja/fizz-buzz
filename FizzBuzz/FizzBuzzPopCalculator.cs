﻿namespace FizzBuzz
{
    /// <summary>
    /// A variation of the classic game.  Maintains all functionality of FizzBuzz <see cref="FizzBuzzCalculator"/>.
    /// But also replaces multiples of 7 with pop.
    /// </summary>
    public class FizzBuzzPopCalculator : FizzBuzzCalculator
    {
        public override string CalculateScore(int number)
        {
            // the result from the base fizz buzz game
            string result = FizzBuzz(number);

            // add pop if the number is a multiple of 7
            if (number.IsMultipleOf(7))
            {
                // if fizz, buzz, or fizz buzz is already in the result, add a space
                if (result.Length > 0)
                {
                    result = result + " ";
                }
                result = result + FizzBuzzConstants.Pop;
            }

            // If result is empty then this is a normal number and the input should be returned.
            // Otherwise, return the result.
            return result.Length == 0 ? number.ToString() : result;
        }
    }
}
