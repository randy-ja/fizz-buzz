﻿namespace FizzBuzz
{
    /// <summary>
    /// Constants to be used throughout the application.
    /// </summary>
    public class FizzBuzzConstants
    {
        public static readonly string Fizz = "fizz";
        public static readonly string Buzz = "buzz";
        public static readonly string Pop = "pop";

        public static readonly string FizzBuzz = "fizz buzz";
        public static readonly string FizzPop = "fizz pop";
        public static readonly string BuzzPop = "buzz pop";
        public static readonly string FizzBuzzPop = "fizz buzz pop";
    }
}
