﻿namespace FizzBuzz
{
    public static class IntegerExtensions
    {
        /// <summary>
        /// Checks if the integer is a multiple of the provided dividend.
        /// </summary>
        /// <param name="number">this number</param>
        /// <param name="dividend">the dividend to check</param>
        /// <returns>true if this is a multiple of the dividend</returns>
        public static bool IsMultipleOf(this int number, int dividend)
        {
            return number % dividend == 0;
        }
    }
}
