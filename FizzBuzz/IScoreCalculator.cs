﻿namespace FizzBuzz
{
    /// <summary>
    /// The interface that all score calculators should inherit from.
    /// </summary>
    public interface IScoreCalculator
    {
        /// <summary>
        /// Calculates the score for the game.
        /// </summary>
        /// <param name="number">The input into the calculator.</param>
        /// <returns>The score.</returns>
        string CalculateScore(int number);
    }
}
