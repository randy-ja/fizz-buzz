﻿namespace FizzBuzz
{
    /// <summary>
    /// The classic Fizz Buzz game.  Multiples of 3 are replaced with fizz.
    /// Multiples of 5 are replaced with buzz.
    /// </summary>
    public class FizzBuzzCalculator : IScoreCalculator
    {
        /// <inheritdoc cref="IScoreCalculator.CalculateScore(int)" />
        public virtual string CalculateScore(int number)
        {
            string result = FizzBuzz(number);

            // If result is empty then this is a normal number and the input should be returned.
            // Otherwise, return the result.
            return result.Length == 0 ? number.ToString() : result;
        }

        /// <summary>
        /// Replaces 3 with Fizz and 5 with Buzz.
        /// </summary>
        /// <param name="number">The number input.</param>
        /// <returns>The result of the calulcation.</returns>
        protected string FizzBuzz(int number)
        {
            string result = string.Empty;

            if (number.IsMultipleOf(3))
            {
                result = FizzBuzzConstants.Fizz;
            }

            if (number.IsMultipleOf(5))
            {
                // add a space if "fizz" was already added to the result
                if (result.Length > 0)
                {
                    result = string.Concat(result, " ");
                }
                result = string.Concat(result, FizzBuzzConstants.Buzz);
            }
            return result;
        }
    }
}
