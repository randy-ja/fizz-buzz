﻿using FizzBuzz;
using NUnit.Framework;

namespace FizzBuzzUnitTests
{
    [TestFixture]
    public class FizzBuzzPopTests
    {
        FizzBuzzPopCalculator calculator;

        [SetUp]
        public void Setup()
        {
            calculator = new FizzBuzzPopCalculator();
        }


        [TestCase(7)]
        [TestCase(28)]
        [TestCase(77)]
        public void Multiples_Of_7_Return_Pop(int number)
        {
            string result = calculator.CalculateScore(number);

            Assert.That(result, Is.EqualTo(FizzBuzzConstants.Pop));
        }


        [TestCase(21)]
        [TestCase(63)]
        [TestCase(126)]
        public void Multiples_Of_3_And_7_Return_FizzPop(int number)
        {
            string result = calculator.CalculateScore(number);

            Assert.That(result, Is.EqualTo(FizzBuzzConstants.FizzPop));
        }


        [TestCase(35)]
        [TestCase(70)]
        [TestCase(140)]
        public void Multiples_Of_5_And_7_Return_BuzzPop(int number)
        {
            string result = calculator.CalculateScore(number);

            Assert.That(result, Is.EqualTo(FizzBuzzConstants.BuzzPop));
        }


        [TestCase(105)]
        [TestCase(210)]
        [TestCase(315)]
        public void Multiples_Of_3_And_5_And_7_Return_FizzBuzzPop(int number)
        {
            string result = calculator.CalculateScore(number);

            Assert.That(result, Is.EqualTo(FizzBuzzConstants.FizzBuzzPop));
        }
    }
}
