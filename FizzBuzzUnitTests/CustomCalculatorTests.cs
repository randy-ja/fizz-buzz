﻿using FizzBuzz;
using NUnit.Framework;
using System.Collections.Generic;

namespace FizzBuzzUnitTests
{
    [TestFixture]
    public class CustomCalculatorTests
    {
        CustomCalculator calculator;
        string fuzz = "fuzz";
        string bizz = "bizz";
        string fuzzBizz = "fuzz bizz";

        [SetUp]
        public void SetUp()
        {
            IDictionary<int, string> replacements = new Dictionary<int, string>
            {
                { 2, fuzz }, // 2 should be replaced by fuzz
                { 3, bizz } // 3 should be replaced by bizz
            };
            calculator = new CustomCalculator(replacements);
        }


        [TestCase(4)]
        [TestCase(16)]
        [TestCase(152)]
        public void Multiples_Of_2_Return_Fuzz(int number)
        {
            string result = calculator.CalculateScore(number);

            Assert.That(result, Is.EqualTo(fuzz));
        }


        [TestCase(9)]
        [TestCase(93)]
        [TestCase(321)]
        public void Multiples_Of_3_Return_Bizz(int number)
        {
            string result = calculator.CalculateScore(number);

            Assert.That(result, Is.EqualTo(bizz));
        }


        [TestCase(6)]
        [TestCase(48)]
        [TestCase(150)]
        public void Multiples_Of_2_And_3_Return_FuzzBizz(int number)
        {
            string result = calculator.CalculateScore(number);

            Assert.That(result, Is.EqualTo(fuzzBizz));
        }



        [TestCase(5)]
        [TestCase(91)]
        [TestCase(317)]
        public void Normal_Numbers_Return_Same_Number(int number)
        {
            string result = calculator.CalculateScore(number);

            Assert.That(result, Is.EqualTo(number.ToString()));
        }
    }
}
