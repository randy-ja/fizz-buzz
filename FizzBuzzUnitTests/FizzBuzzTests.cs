using FizzBuzz;
using NUnit.Framework;

namespace FizzBuzzUnitTests
{
    [TestFixture(typeof(FizzBuzzCalculator))]
    [TestFixture(typeof(FizzBuzzPopCalculator))]
    public class FizzBuzzTests<T> where T : IScoreCalculator, new()
    {
        T calculator;

        [SetUp]
        public void Setup()
        {
            calculator = new T();
        }


        [TestCase(1)]
        [TestCase(2)]
        [TestCase(4)]
        public void Normal_Numbers_Return_SameNumber(int input)
        {
            string result = calculator.CalculateScore(input);

            Assert.That(result, Is.EqualTo(input.ToString()));
        }


        [TestCase(3)]
        [TestCase(9)]
        [TestCase(123)]
        public void Multiples_Of_3_Return_Fizz(int input)
        {
            string result = calculator.CalculateScore(input);

            Assert.That(result, Is.EqualTo(FizzBuzzConstants.Fizz));
        }


        [TestCase(5)]
        [TestCase(20)]
        [TestCase(200)]
        public void Multiples_Of_5_Return_Buzz(int input)
        {
            string result = calculator.CalculateScore(input);

            Assert.That(result, Is.EqualTo(FizzBuzzConstants.Buzz));
        }


        [TestCase(15)]
        [TestCase(45)]
        [TestCase(285)]
        public void Multiples_Of_3_And_5_Return_FizzBuzz(int input)
        {
            string result = calculator.CalculateScore(input);

            Assert.That(result, Is.EqualTo(FizzBuzzConstants.FizzBuzz));
        }
    }
}